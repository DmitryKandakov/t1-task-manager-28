package ru.t1.dkandakov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Setter
@Getter
public class AbstractUserOwnedModel extends AbstractModel {

    @Getter
    @Setter
    @Nullable
    private String userId;

}
