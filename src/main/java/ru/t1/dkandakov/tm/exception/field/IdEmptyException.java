package ru.t1.dkandakov.tm.exception.field;

public final class IdEmptyException extends AbstractFildException {

    public IdEmptyException() {
        super("Error! Id is empty...");
    }

}
