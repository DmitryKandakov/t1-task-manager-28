package ru.t1.dkandakov.tm.exception.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.exception.AbstractException;

@NoArgsConstructor
public class AbstractSystemException extends AbstractException {

    public AbstractSystemException(@NotNull final String command) {
        super(command);
    }

    public AbstractSystemException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    public AbstractSystemException(@NotNull final Throwable cause) {
        super(cause);
    }

    public AbstractSystemException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
