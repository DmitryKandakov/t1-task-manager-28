package ru.t1.dkandakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.dkandakov.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "logout";

    @NotNull
    private final String DESCRIPTION = "logout current user";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
