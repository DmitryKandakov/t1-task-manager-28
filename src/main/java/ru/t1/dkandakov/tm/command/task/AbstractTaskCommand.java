package ru.t1.dkandakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.api.service.IProjectTaskService;
import ru.t1.dkandakov.tm.api.service.ITaskService;
import ru.t1.dkandakov.tm.command.AbstractCommand;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.enumerated.Status;
import ru.t1.dkandakov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

    protected void renderTasks(final List<Task> tasks) {
        int index = 1;
        for (@Nullable final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + ". " + task);
            index++;
        }
    }

    protected void showTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: + " + task.getId());
        System.out.println("NAME: + " + task.getName());
        System.out.println("DESCRIPTION: + " + task.getDescription());
        System.out.println("STATUS: + " + Status.toName(task.getStatus()));
    }

}