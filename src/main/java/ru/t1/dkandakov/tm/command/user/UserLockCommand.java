package ru.t1.dkandakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkandakov.tm.enumerated.Role;
import ru.t1.dkandakov.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-lock";

    @NotNull
    private final String DESCRIPTION = "lock user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        getUserService().lockUserByLogin(login);
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
