package ru.t1.dkandakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[CLEAR TASKS]");
        @Nullable final String userId = getUserId();
        getTaskService().removeAll(userId);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

}